import sys
import os
import gspread
import psycopg2
import time
import datetime
from oauth2client.service_account import ServiceAccountCredentials
from itertools import islice
from dotenv import load_dotenv
from re import sub
from decimal import Decimal
from os.path import join, dirname
from apscheduler.schedulers.blocking import BlockingScheduler

def conn():
    dotenv_path = join(dirname(__file__), '.env')
    load_dotenv(dotenv_path)
    dbhost = os.environ.get("DB_HOST")
    dbuser = os.environ.get("DB_USER")
    dbpass = os.environ.get("DB_PASSWORD")
    dbport = os.environ.get("DB_PORT")
    database = os.environ.get("DB_DATABASE")

    db_conn = psycopg2.connect(
        dbname=database, user=dbuser, host=dbhost, password=dbpass, port=dbport)
    return db_conn

def order():
    start_time = datetime.datetime.now()
    print('Cron job started at', start_time.strftime('%H:%M:%S.%f')[:-4])

    scope = ['https://spreadsheets.google.com/feeds',
            'https://www.googleapis.com/auth/drive']
    creds = ServiceAccountCredentials.from_json_keyfile_name(
        'binar-insight.json', scope)
    client = gspread.authorize(creds)
    sheet = client.open("Table for non-app transaction").sheet1

    cursor = conn().cursor()

    # iterate column to start reading from second top row
    order_count_sql = 'SELECT COUNT(*) FROM orders'
    cursor.execute(order_count_sql)
    count_result = cursor.fetchone()
    num_rows_orders = count_result[0]
    num_rows_sheets = len(sheet.get_all_values())
    num_rows_sheets = num_rows_sheets - 1

    iterable_results = iter(sheet.get_all_values())
    next(iterable_results)

    accepted_count = 0
    denied_count = 0

    for i, value in islice(enumerate(iterable_results), num_rows_orders, None):
        find_existing = (
            "SELECT COUNT(*) FROM orders WHERE (invoice_number) = '{0}'")
        cursor.execute(find_existing.format(value[0]))
        data = cursor.fetchone()

        if data[0] == 0:
            invoice_number = value[0]
            user_id = get_user(value[1])
            slip_verified_at = "" if not value[3] else value[3]
            order_type = "" if not value[4] else value[4]
            initial_amount = "" if not value[8] else currency_to_float(value[8])
            amount = "" if not value[9] else currency_to_float(value[9])
            payment_method_id = get_payment_method(value[7])
            created_at = datetime.datetime.now()
            updated_at = datetime.datetime.now()
            # TODO: missing column: promo, channle, source
            # cannot add missing column since there is no targeted column on orders table

            query = (
                "INSERT INTO orders (invoice_number, user_id, order_type, payment_method_id, slip_verified_at, created_at, updated_at, initial_amount, amount)"
                "VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)")
            with conn() as connection:
                with connection.cursor() as curs:
                    curs.execute(query, (invoice_number, 
                                        user_id, 
                                        order_type,
                                        payment_method_id, 
                                        slip_verified_at, 
                                        created_at, 
                                        updated_at, 
                                        initial_amount, 
                                        amount))

            accepted_count += 1
            print('ACCEPTED :', value[0], 'Successfully stored', datetime.datetime.now())
        else:
            denied_count += 1
            print('DENIED :', value[0], 'Appear to be duplicated data', datetime.datetime.now())

    if accepted_count > 0:
        print("Total accepted data:", accepted_count)
    if denied_count > 0:
        print("Total denied data:", denied_count)

    end_time = datetime.datetime.now()
    print('Cron job ended at', end_time.strftime('%H:%M:%S.%f')[:-4])
    print('\nElapsed time', end_time - start_time)

def currency_to_float(val):
    money = Decimal(sub(r'[^\d.]', '', val))
    return money

def get_user(email):
    cursor = conn().cursor()
    find_user = ("SELECT id FROM users WHERE (email) = '{0}'")
    cursor.execute(find_user.format(email))
    data = cursor.fetchone()
    return data

def get_payment_method(val):
    cursor = conn().cursor()
    find_payment = ("SELECT id FROM payment_methods WHERE (name) = '{0}'")
    cursor.execute(find_payment.format(val))
    data = cursor.fetchone()
    return data


print("Python version")
print (sys.version)

sched = BlockingScheduler()

sched.add_job(order, 'cron', hour=12)
sched.start()
